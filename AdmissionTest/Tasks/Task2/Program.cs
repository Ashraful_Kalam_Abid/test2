﻿using System;

namespace Task2
{
    public class Program
    {
        static void Main(string[] args)
        {
            var text = "aaabbcc";
            var result = text.Rotate(2);
            Console.WriteLine(result);
        }
    }

    public static class StringExtension{
        public static string Rotate(this string str, int d)
        {
            String ans = str.Substring(d, str.Length - d) + str.Substring(0, d);
            return ans;

        }
    }
    /* Write your code below this line*/
}
