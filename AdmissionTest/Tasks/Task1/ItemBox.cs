﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    public class ItemBox<T> : IStorage<T> where T : class, new()
    {
        // Write your code here
        public List<T> list = new List<T>();
        public int Count { get => Count; set => Count++; }
        
        public void Add(T item)
        {
            list.Add(item);
 
        }

        public T Get(int index)
        {
            return list[index];
        }

        public void Remove(T item)
        {
            list.Remove(item);

        }
    }
}
