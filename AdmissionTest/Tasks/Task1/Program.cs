﻿using System;

namespace Task1
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Show example use case of ItemBox
            ItemBox<TestClass> a = new ItemBox<TestClass>();
            //Console.WriteLine(a.Count);

            a.Add(new TestClass { MyValue = "MyTest Value" });
            a.Add(new TestClass { MyValue = "MyTest Value2" });

            //Console.WriteLine(a.Count);

            Console.WriteLine(a.Get(0).MyValue);
            Console.WriteLine(a.Get(1).MyValue);

            a.Remove(new TestClass { MyValue = "MyTest Value2" });
            //Console.WriteLine(a.Count);





        }
    }
    public class TestClass
    {
        public string MyValue { get; set; }
    }
}
